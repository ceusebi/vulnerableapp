﻿using System;
using System.Text;

namespace WasmExample.Shared.Extensions
{
	public static class StringExtensions
	{
		public static string ToBase64(this string text) => Convert.ToBase64String(Encoding.UTF8.GetBytes(text));

		public static string FromBase64(this string encoded) => Encoding.UTF8.GetString(Convert.FromBase64String(encoded));
	}
}
