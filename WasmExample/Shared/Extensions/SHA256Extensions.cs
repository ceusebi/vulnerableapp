﻿using System.Security.Cryptography;
using System.Text;

namespace WasmExample.Shared.Extensions
{
	public static class SHA256Extensions
	{
		public static string ComputeHash(this SHA256 hasher, string rawData)
		{
			var hashedBytes = hasher.ComputeHash(Encoding.UTF8.GetBytes(rawData));
			var sb = new StringBuilder();
			for (var i = 0; i < hashedBytes.Length; i++)
			{
				sb.Append(hashedBytes[i].ToString("x2"));
			}
			return sb.ToString();
		}
	}
}
