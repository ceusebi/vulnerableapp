﻿using System;

namespace WasmExample.Shared
{
	public class ChatMessage
	{
		public string User { get; set; } = string.Empty;
		public string Message { get; set; } = string.Empty;
		public DateTime EmmitionDate { get; set; } = DateTime.UtcNow;
	}
}
