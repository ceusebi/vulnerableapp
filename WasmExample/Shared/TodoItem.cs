﻿namespace WasmExample.Shared
{
	public class TodoItem
	{
		public string? Title { get; set; }
		public bool? IsDone { get; set; }
	}
}
