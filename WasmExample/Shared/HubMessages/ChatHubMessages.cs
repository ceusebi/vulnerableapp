﻿namespace WasmExample.Shared.HubMessages
{
	public static class ChatHubMessages
	{
		public static readonly string BaseUrl = "/chatHub";
		public static readonly string Receive = "ReceiveMessage";
		public static readonly string Send = "SendMessage";
		public static readonly string GetHistory = "GetHistory";
	}
}
