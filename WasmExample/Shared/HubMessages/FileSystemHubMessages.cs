﻿namespace WasmExample.Shared.HubMessages
{
	public static class FileSystemHubMessages
	{
		public static readonly string BaseUrl = "/fileSystemHub";
		public static readonly string DirectoryChangedMessage = "DirectoryChanged";
		public static readonly string CurrentState = "CurrentState";
	}
}
