﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using WasmExample.Shared.Extensions;

namespace WasmExample.Shared
{

	public static class FileSystemTree
	{
		public static FileSystemDirectory MapDirectory(string root)
		{
			var dirInfo = new DirectoryInfo(root);
			return new FileSystemDirectory
			{
				Name = dirInfo.Name,
				Link = dirInfo.FullName.ToBase64(),
				Files = dirInfo.EnumerateFiles().Select(fi => new FileSystemFile
				{
					Name = fi.Name,
					Link = fi.FullName.ToBase64()
				}).ToList(),
				Directories = dirInfo.EnumerateDirectories().Select(di => MapDirectory(di.FullName)).ToList()
			};
		}
	}
	public class FileSystemDirectory
	{
		public string Name { get; set; } = string.Empty;
		public string Link { get; set; } = string.Empty;
		public List<FileSystemDirectory> Directories = new List<FileSystemDirectory>();
		public List<FileSystemFile> Files = new List<FileSystemFile>();
	}

	public class FileSystemFile
	{
		public string Name { get; set; } = string.Empty;
		public string Link { get; set; } = string.Empty;
	}
}
