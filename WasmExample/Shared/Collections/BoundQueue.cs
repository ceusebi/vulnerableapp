﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace WasmExample.Shared.Collections
{
	public class BoundQueue<T>: IProducerConsumerCollection<T>, IEnumerable<T>, IEnumerable, ICollection, IReadOnlyCollection<T>
	{
		private ConcurrentQueue<T> BackingQueue { get; } 
		private long Limit { get; }
		private object SkimCs { get; } = new { };
		private bool IsFull => BackingQueue.Count > Limit;

		// Summary:
		//     Initializes a new instance of the WasmExample.Shared.Collections.BoundQueue`1
		//     class.
		//
		// Parameters:
		//   limit:
		//     The maximum ammount of items this bound queue accepts.
		public BoundQueue(long limit)
		{
			Limit = limit;
			BackingQueue = new ConcurrentQueue<T>();
		}
		// Summary:
		//     Initializes a new instance of the WasmExample.Shared.Collections.BoundQueue`1
		//     class that contains elements copied from the specified collection
		//
		// Parameters:
		//   collection:
		//     The collection whose elements are copied to the new  WasmExample.Shared.Collections.BoundQueue`1.
		//   limit:
		//     The maximum ammount of items this bound queue accepts.
		//
		// Exceptions:
		//   T:System.ArgumentNullException:
		//     The collection argument is null.
		public BoundQueue(IEnumerable<T> collection, long limit)
		{
			Limit = limit;
			BackingQueue = new ConcurrentQueue<T>(collection);
			Skim();
		}
		//
		// Summary:
		//     Gets the number of elements contained in the  WasmExample.Shared.Collections.BoundQueue`1.
		//
		// Returns:
		//     The number of elements contained in the  WasmExample.Shared.Collections.BoundQueue`1.
		public int Count => BackingQueue.Count;
		//
		// Summary:
		//     Gets a value that indicates whether the  WasmExample.Shared.Collections.BoundQueue`1
		//     is empty.
		//
		// Returns:
		//     true if the  WasmExample.Shared.Collections.BoundQueue`1 is empty; otherwise,
		//     false.
		public bool IsEmpty => BackingQueue.IsEmpty;
		//
		// Summary:
		//     Removes all objects from the  WasmExample.Shared.Collections.BoundQueue`1.
		public void Clear() => BackingQueue.Clear();
		//
		// Summary:
		//     Copies the  WasmExample.Shared.Collections.BoundQueue`1 elements to an existing
		//     one-dimensional System.Array, starting at the specified array index.
		//
		// Parameters:
		//   array:
		//     The one-dimensional System.Array that is the destination of the elements copied
		//     from the  WasmExample.Shared.Collections.BoundQueue`1. The System.Array must
		//     have zero-based indexing.
		//
		//   index:
		//     The zero-based index in array at which copying begins.
		//
		// Exceptions:
		//   T:System.ArgumentNullException:
		//     array is a null reference (Nothing in Visual Basic).
		//
		//   T:System.ArgumentOutOfRangeException:
		//     index is less than zero.
		//
		//   T:System.ArgumentException:
		//     index is equal to or greater than the length of the array -or- The number of
		//     elements in the source  WasmExample.Shared.Collections.BoundQueue`1 is greater
		//     than the available space from index to the end of the destination array.
		public void CopyTo(T[] array, int index) => BackingQueue.CopyTo(array, index);
		//
		// Summary:
		//     Adds an object to the end of the  WasmExample.Shared.Collections.BoundQueue`1.
		//
		// Parameters:
		//   item:
		//     The object to add to the end of the  WasmExample.Shared.Collections.BoundQueue`1.
		//     The value can be a null reference (Nothing in Visual Basic) for reference types.
		public void Enqueue(T item)
		{
			lock(SkimCs)
			{
				BackingQueue.Enqueue(item);
				Skim();
			}
		}
		//
		// Summary:
		//     Returns an enumerator that iterates through the  WasmExample.Shared.Collections.BoundQueue`1.
		//
		// Returns:
		//     An enumerator for the contents of the  WasmExample.Shared.Collections.BoundQueue`1.
		public IEnumerator<T> GetEnumerator() => BackingQueue.GetEnumerator();
		//
		// Summary:
		//     Copies the elements stored in the  WasmExample.Shared.Collections.BoundQueue`1
		//     to a new array.
		//
		// Returns:
		//     A new array containing a snapshot of elements copied from the  WasmExample.Shared.Collections.BoundQueue`1.
		public T[] ToArray() => BackingQueue.ToArray();
		//
		// Summary:
		//     Tries to remove and return the object at the beginning of the concurrent queue.
		//
		// Parameters:
		//   result:
		//     When this method returns, if the operation was successful, result contains the
		//     object removed. If no object was available to be removed, the value is unspecified.
		//
		// Returns:
		//     true if an element was removed and returned from the beginning of the  WasmExample.Shared.Collections.BoundQueue`1
		//     successfully; otherwise, false.
		public bool TryDequeue(out T result) => BackingQueue.TryDequeue(out result);
		//
		// Summary:
		//     Tries to return an object from the beginning of the  WasmExample.Shared.Collections.BoundQueue`1
		//     without removing it.
		//
		// Parameters:
		//   result:
		//     When this method returns, result contains an object from the beginning of the
		//      WasmExample.Shared.Collections.BoundQueue`1 or an unspecified value if the
		//     operation failed.
		//
		// Returns:
		//     true if an object was returned successfully; otherwise, false.
		public bool TryPeek(out T result) => BackingQueue.TryDequeue(out result);
		private void Skim()
		{
			var ok = true;
			while(ok && IsFull)
			{
				ok = BackingQueue.TryDequeue(out var _);
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)BackingQueue).GetEnumerator();
		}
		public bool IsSynchronized => ((ICollection)BackingQueue).IsSynchronized;
		public object SyncRoot => ((ICollection)BackingQueue).SyncRoot; 
		public void CopyTo(Array array, int index) => ((ICollection) BackingQueue).CopyTo(array, index);
		public bool TryAdd(T item)
		{
			try { Enqueue(item); }
			catch(Exception) { return false; }
			return true;
		}
		public bool TryTake(out T item)
		{
			return ((IProducerConsumerCollection<T>)BackingQueue).TryTake(out item);
		}
	}
}
