﻿using Microsoft.JSInterop;
using System;
using System.Threading.Tasks;

namespace WasmExample.Client.IJSRuntimeExtensions
{
	public static partial class SaveFileExtension
	{
		//Implemented in wwwroot/js/SaveAsFile.cs
		public static ValueTask<object> SaveFile(this IJSRuntime js, string filename, byte[] data)
			=> js.InvokeAsync<object>("SaveAsFile", filename, Convert.ToBase64String(data));
	}
}
