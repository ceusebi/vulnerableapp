﻿using Microsoft.AspNetCore.Blazor.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace WasmExample.Client
{
	public sealed class Program
	{
		public static async Task Main(string[] args)
		{
			var builder = WebAssemblyHostBuilder.CreateDefault(args);
			builder
				.ConfigureServices()
				.RootComponents
					.Add<App>("app");

			await builder.Build().RunAsync();
		}
	}

	public static class WebAssemblyHostBuilderExtensions
	{
		public static WebAssemblyHostBuilder ConfigureServices(this WebAssemblyHostBuilder hostBuilder)
		{
			ConfigureServices(hostBuilder.Services);
			return hostBuilder;
		}

		private static void ConfigureServices(IServiceCollection services)
		{

		}
	}
}
