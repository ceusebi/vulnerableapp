﻿using Microsoft.AspNetCore.Mvc;
using System.IO;
using WasmExample.Shared.Extensions;

namespace WasmExample.Server.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class DownloadController : Controller
	{
		[HttpGet("{encoded}")]
		public IActionResult GetFile(string encoded)
		{
			if (!ModelState.IsValid) return BadRequest();
			var file = encoded.FromBase64();
			var fileName = Path.GetFileName(file);
			var normalizedPath = Path.GetFullPath(file);
			// Do not use 'using' since it'll close the stream before returning to client.
			// File() disposes on its own.
			var fileStream = System.IO.File.OpenRead(normalizedPath);
			return File(fileStream, "application/octet-stream", fileName);
		}
	}
}
