﻿using WasmExample.Shared;
using WasmExample.Shared.Collections;

namespace WasmExample.Server.Etc
{
	public class ChatMessageHistory
	{
		public BoundQueue<ChatMessage> Messages { get; } = new BoundQueue<ChatMessage>(10);
	}
}
