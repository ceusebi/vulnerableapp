﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WasmExample.Server.Configs;
using WasmExample.Server.Hubs;

namespace WasmExample.Server.Services
{
	public class UploadsWatcher : IHostedService, IDisposable
	{
		private FileSystemWatcher Watcher { get; }
		private IHubContext<FileSystemHub> FileSystemHubContext { get; }
		private ILogger Logger { get; }


		public UploadsWatcher(IHubContext<FileSystemHub> fileSystemHub, FileSystemConfiguration config, ILogger<UploadsWatcher> logger)
		{
			FileSystemHubContext = fileSystemHub;
			Logger = logger;

			Watcher = new FileSystemWatcher()
			{
				Path = config.BaseDirectory,
				IncludeSubdirectories = true,
			};

			Watcher.Changed += OnFileSystemChanged;
			Watcher.Renamed += OnFileSystemChanged;
			Watcher.Created += OnFileSystemChanged;
			Watcher.Deleted += OnFileSystemChanged;
			Logger.LogDebug($"Registered upload watcher events.");
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			Watcher.EnableRaisingEvents = true;
			Logger.LogDebug($"Started watching uploads file system");
			return Task.CompletedTask;
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			Dispose();
			Logger.LogDebug($"Stopped watching uploads file system");
			return Task.CompletedTask;
		}

		public void Dispose()
		{
			Watcher.Dispose();
		}

		private void OnFileSystemChanged(object _, FileSystemEventArgs _1) => FileSystemHub.NotifyChange(FileSystemHubContext);
	}
}
