﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WasmExample.Server.Configs;
using WasmExample.Server.Services;

namespace WasmExample.Server
{
	public class Program
	{
		public static void Main(string[] args)
		{
			BuildWebHost(args).Run();
		}

		public static IWebHost BuildWebHost(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseConfiguration(new ConfigurationBuilder()
					.AddCommandLine(args)
					.Build())
				.UseStartup<Startup>()
				.ConfigureServices(AddExtraServices)
				.Build();

		private static void AddExtraServices(IServiceCollection services)
		{
			services.AddSingleton<FileSystemConfiguration>();
			services.AddHostedService<UploadsWatcher>();
		}
	}
}
