﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;

namespace WasmExample.Server.Configs
{
	public class FileSystemConfiguration
	{
		public string BaseDirectory { get; }
		private ILogger Logger { get; }
		public FileSystemConfiguration(ILogger<FileSystemConfiguration> logger)
		{
			Logger = logger;
			var home = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			BaseDirectory = Path.GetFullPath(Path.Combine(home, "Uploads"));
			var dirInfo = new DirectoryInfo(BaseDirectory);
			var existed = dirInfo.Exists;
			dirInfo.Create();
			Logger.LogDebug($"Folder {BaseDirectory} was {(existed ? "found" : "created")}");
		}
	}
}
