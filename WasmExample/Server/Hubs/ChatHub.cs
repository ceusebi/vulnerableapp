﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;
using WasmExample.Server.Etc;
using WasmExample.Shared;
using WasmExample.Shared.HubMessages;

namespace WasmExample.Server.Hubs
{
	public class ChatHub : Hub
	{
		private ChatMessageHistory History { get; }
		private ILogger Logger { get; }
		public ChatHub(ChatMessageHistory chatMessageHistory, ILogger<ChatHub> logger)
		{
			History = chatMessageHistory;
			Logger = logger;
		}
		public async Task SendMessage(ChatMessage message)
		{
			Logger.LogDebug($"Adding message of user {message.User}");
			History.Messages.Enqueue(message);
			await Clients.All.SendAsync(ChatHubMessages.Receive, message);
		}

		public async Task<IEnumerable<ChatMessage>> GetHistory() => await Task.FromResult(History.Messages);
	}

}
