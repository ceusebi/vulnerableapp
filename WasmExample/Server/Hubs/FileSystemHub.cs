﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;
using WasmExample.Server.Configs;
using WasmExample.Shared;
using WasmExample.Shared.HubMessages;

namespace WasmExample.Server.Hubs
{
	public class FileSystemHub : Hub
	{
		private FileSystemConfiguration Config { get; }
		private ILogger Logger { get; }
		public FileSystemHub(FileSystemConfiguration config, ILogger<FileSystemHub> logger)
		{
			Config = config;
			Logger = logger;
		}

		public async Task<FileSystemDirectory> CurrentState() => await Task.Run(() => FileSystemTree.MapDirectory(Config.BaseDirectory));

		internal static void NotifyChange(IHubContext<FileSystemHub> hubContext) => hubContext.Clients.All.SendAsync(FileSystemHubMessages.DirectoryChangedMessage);
	}
}
